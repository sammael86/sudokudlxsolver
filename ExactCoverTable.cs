namespace SudokuDLXSolver;

public class ExactCoverTable
{
    private readonly int _grade;
    private readonly int _square;
    private readonly int _fourth;

    private readonly long[] _columns;

    private readonly string _cacheDirectory;

    public ExactCoverTable(int grade)
    {
        _grade = grade;
        _square = _grade * _grade;
        _fourth = _square * _square;

        _columns = new long[_fourth * 4];

        _cacheDirectory = Path.Combine(Directory.GetCurrentDirectory(), "cache");
        if (!Directory.Exists(_cacheDirectory))
            Directory.CreateDirectory(_cacheDirectory);
    }

    public IEnumerable<long> GetColumnNames()
    {
        // cells
        for (long k = 0; k < _fourth; k++)
        {
            var nameValue = _columns[k] = (k << 32) | k;
            yield return nameValue;
        }

        // rows
        for (long k = 0; k < _fourth; k++)
        {
            var nameValue = _columns[k + _fourth] = ((k / _square + _fourth) << 32) | (k % _square + 1);
            yield return nameValue;
        }

        // cols
        for (long k = 0; k < _fourth; k++)
        {
            var nameValue = _columns[k + _fourth * 2] = ((k / _square + _fourth * 2) << 32) | (k % _square + 1);
            yield return nameValue;
        }

        // quads
        for (long k = 0; k < _fourth; k++)
        {
            var nameValue = _columns[k + _fourth * 3] = ((k / _square + _fourth * 3) << 32) | (k % _square + 1);
            yield return nameValue;
        }
    }

    public IEnumerable<long[]> GetExactCoverTable()
    {
        var path = Path.Combine(_cacheDirectory, $"sudoku-{_square}x{_square}.sdk");

        if (File.Exists(path))
            foreach (var row in File.ReadAllLines(path))
                yield return row.Split().Select(long.Parse).ToArray();
        else
        {
            using var file = new StreamWriter(path);
            for (var k = 0; k < _square * _fourth; k++)
            {
                var row = new long[4];

                // cells
                for (var i = 0; i < _fourth; i++)
                    if (k / _square == i)
                        row[0] = _columns[i];

                // rows
                for (var i = 0; i < _fourth; i++)
                    if (k % _square + _square * (k / _fourth) == i)
                        row[1] = _columns[i + _fourth];

                // cols
                for (var i = 0; i < _fourth; i++)
                    if (k % _fourth == i)
                        row[2] = _columns[i + _fourth * 2];

                // quads
                for (var i = 0; i < _fourth; i++)
                    if (k % _square + _square *
                        (k / _square / _grade - _grade * (k / _fourth) + _grade * (k / _fourth / _grade)) == i)
                        row[3] = _columns[i + _fourth * 3];

                file.WriteLine(string.Join(' ', row));
                yield return row;
            }
        }
    }
}