﻿using System.Diagnostics;
using SudokuDLXSolver.Structures;

namespace SudokuDLXSolver;

internal static class Program
{
    private static long _updates;
    private static int _level;

    private static ExactCoverTable _exactCoverTable = null!;
    private static long[,]? _rowData;
    private static readonly Column Root = new();

    private static bool _verbose;
    private static long _solutionsCount;
    private static int _grade;
    private static int _square;
    private static int _fourth;
    private static int _maximumLevel;

    private static int[] _initialLayout = null!;
    private static Column[] _bestColumns = null!;
    private static Random _r = new();

    private static FactorArray<Column> _knownColumns = null!;
    private static FactorArray<Node> _knownNodes = null!;

    public static void Main()
    {
        bool repeat;
        do
        {
            _updates = 0;
            _level = 0;
            _rowData = null;
            _solutionsCount = 0;
            _r = new();

            Console.Write("Enter sudoku grade (default is 3) [2-7]: ");
            _grade = int.TryParse(Console.ReadLine(), out var grade) && grade is >= 2 and <= 7 ? grade : 3;
            _square = _grade * _grade;
            _fourth = _square * _square;

            Console.Write("Print solutions (up to 10) or only summary? [Y/n]: ");
            var answer = Console.ReadLine();
            _verbose = string.Compare(answer, "n", StringComparison.InvariantCultureIgnoreCase) != 0;

            _exactCoverTable = new(_grade);
            _initialLayout = new int[_fourth];
            _bestColumns = new Column[_fourth * 4];

            _knownColumns = new(_fourth);
            _knownNodes = new(_fourth);

            InitializeExactCoverTable();
            GetSudokuLayout();
            RemoveKnownDigits();

            if (_maximumLevel != _fourth)
                SolveSudoku();
            else
                GenerateSudoku();

            Console.Write("\nRepeat? [Y/n]: ");
            answer = Console.ReadLine();
            repeat = string.Compare(answer, "n", StringComparison.InvariantCultureIgnoreCase) != 0;
        } while (repeat);
    }

    private static void SolveSudoku()
    {
        Console.WriteLine("\nYour sudoku:");
        PrintSudoku(new int[_fourth]);

        var sw = Stopwatch.StartNew();

        foreach (var solution in FindAllSolutions())
            if (_verbose)
            {
                Console.WriteLine($"\nSolution {_solutionsCount} (total {_updates:N0} updates):");
                PrintSudoku(solution);
            }

        sw.Stop();

        Console.WriteLine($"\nTotal {_solutionsCount:N0} solutions, after {_updates:N0} updates.");
        Console.WriteLine($"Elapsed {sw.ElapsedMilliseconds:N0} ms");
    }

    private static void InitializeExactCoverTable()
    {
        Root.Next = Root.Prev = Root;
        AddColumns();

        if (_rowData is null)
        {
            _rowData = new long[_fourth * _square, 4];
            var rowIndex = 0;
            var cellIndex = 0;
            foreach (var row in _exactCoverTable.GetExactCoverTable())
            {
                foreach (var cell in row) _rowData[rowIndex, cellIndex++] = cell;
                rowIndex++;
                cellIndex = 0;
            }
        }

        AddRows();
    }

    private static void AddColumns()
    {
        var currentColumn = Root;

        foreach (var column in _exactCoverTable.GetColumnNames())
        {
            Column newColumn;
            currentColumn = currentColumn.Next = newColumn = new()
            {
                Length = 0,
                NameValue = column,
                Prev = currentColumn
            };
            newColumn.Head.Up = newColumn.Head.Down = newColumn.Head;
        }

        currentColumn.Next = Root;
        Root.Prev = currentColumn;
    }

    private static void AddRows()
    {
        var counter = 0;
        Node node = null;
        foreach (var row in _rowData!)
        {
            var currentColumn = GetColumnByName(row);

            var newNode = new Node
            {
                Column = currentColumn,
                Up = currentColumn.Head.Up,
                Down = currentColumn.Head
            };

            if ((counter & 3) == 0)
            {
                node = newNode;
                node.Right = node.Left = node;
            }
            else
            {
                newNode.Left = node!;
                newNode.Right = node!.Right;
                node.Right = node.Right.Left = newNode;
            }

            currentColumn.Head.Up.Down = newNode;
            currentColumn.Head.Up = newNode;
            currentColumn.Length++;

            node = node.Right;

            counter++;
        }
    }

    private static void GetSudokuLayout()
    {
        Console.WriteLine("Enter sudoku values separated by space:");
        Console.SetIn(new StreamReader(Console.OpenStandardInput(), Console.InputEncoding, false, 8192));
        var line = Console.ReadLine();
        if (string.IsNullOrEmpty(line))
            return;

        var values = line.Split();
        for (var i = 0; i < values.Length; i++)
            if (!string.IsNullOrWhiteSpace(values[i]))
                _initialLayout[i] = int.Parse(values[i]);
    }

    private static void RemoveKnownDigits()
    {
        _maximumLevel = _fourth;

        _knownColumns.Clear();
        _knownNodes.Clear();

        for (var i = 0; i < _fourth; i++)
            if (_initialLayout[i] != 0)
            {
                var column = GetColumnByName(((long)i << 32) | (uint)i);
                var node = column.Head.Down;
                for (var j = 1; j < _initialLayout[i]; node = node.Down, j++) { }

                _knownColumns.Add(column);
                _knownNodes.Add(node);

                _maximumLevel--;
            }

        foreach (var column in _knownColumns)
            HideColumn(column);
        foreach (var node in _knownNodes)
            HideAllColumnsOf(node);
    }

    private static Column GetColumnByName(long name)
    {
        Column column;
        for (column = Root.Next; column.NameValue != name || column == Root; column = column.Next) { }

        return column;
    }

    private static IEnumerable<int[]> FindAllSolutions()
    {
        _level = 0;
        var choice = new Node[_maximumLevel];

        do
        {
            var bestColumn = GetTheBestColumn();
            HideColumn(bestColumn);

            var currentNode = choice[_level] = bestColumn.Head.Down;

            do
            {
                if (currentNode == bestColumn.Head)
                {
                    UnhideColumn(bestColumn);
                    if (_level == 0) yield break;

                    currentNode = choice[--_level];
                    bestColumn = currentNode.Column;

                    UnhideAllColumnsOf(currentNode);
                    choice[_level] = currentNode = currentNode.Down;

                    continue;
                }

                HideAllColumnsOf(currentNode);

                if (Root.Next == Root)
                {
                    _solutionsCount++;
                    if (_verbose && _solutionsCount <= 10) yield return RecordSolution(choice);

                    UnhideAllColumnsOf(currentNode);
                    choice[_level] = currentNode = currentNode.Down;

                    continue;
                }

                _level++;
                break;
            } while (true);
        } while (_level < _maximumLevel);
    }

    private static Column GetTheBestColumn()
    {
        var minLength = int.MaxValue;

        var index = 0;
        for (var currentColumn = Root.Next; currentColumn != Root; currentColumn = currentColumn.Next)
            if (currentColumn.Length == minLength)
                _bestColumns[++index] = currentColumn;
            else if (currentColumn.Length < minLength)
            {
                index = 0;
                _bestColumns[0] = currentColumn;
                minLength = currentColumn.Length;
            }

        return _bestColumns[_r.Next(0, index)];
    }

    private static void HideColumn(Column column)
    {
        var k = 1;

        column.Prev.Next = column.Next;
        column.Next.Prev = column.Prev;

        for (var currentRowNode = column.Head.Down; currentRowNode != column.Head; currentRowNode = currentRowNode.Down)
        for (var currentNode = currentRowNode.Right; currentNode != currentRowNode; currentNode = currentNode.Right)
        {
            currentNode.Up.Down = currentNode.Down;
            currentNode.Down.Up = currentNode.Up;
            currentNode.Column.Length--;
            k++;
        }

        _updates += k;
    }

    private static void HideAllColumnsOf(Node node)
    {
        for (var currentNode = node.Right; currentNode != node; currentNode = currentNode.Right)
            HideColumn(currentNode.Column);
    }

    private static void UnhideColumn(Column column)
    {
        for (var currentRowNode = column.Head.Up; currentRowNode != column.Head; currentRowNode = currentRowNode.Up)
        for (var currentNode = currentRowNode.Left; currentNode != currentRowNode; currentNode = currentNode.Left)
        {
            currentNode.Up.Down = currentNode.Down.Up = currentNode;
            currentNode.Column.Length++;
        }

        column.Prev.Next = column.Next.Prev = column;
    }

    private static void UnhideAllColumnsOf(Node node)
    {
        for (var currentNode = node.Left; currentNode != node; currentNode = currentNode.Left)
            UnhideColumn(currentNode.Column);
    }

    private static int[] RecordSolution(Node[] choice)
    {
        var solution = new int[_fourth];

        for (var i = 0; i <= _level; i++)
        {
            var node = choice[i];

            var currentNode = node;
            var cellIndex = 0;
            var cellValue = 0;
            do
            {
                if (currentNode.Column.Name < _fourth)
                    cellIndex = currentNode.Column.Value;
                else
                    cellValue = currentNode.Column.Value;

                currentNode = currentNode.Right;
            } while (currentNode != node);

            solution[cellIndex] = cellValue;
        }

        return solution;
    }

    private static void PrintSudoku(int[] solution)
    {
        Console.WriteLine("".PadRight(3 * _square + 2 * _grade + 1, '-'));
        for (var i = 0; i < _square; i++)
        {
            Console.Write("|");
            for (var j = 0; j < _square; j++)
            {
                var cellValue = solution[i * _square + j] + _initialLayout[i * _square + j];
                Console.Write($"{(cellValue == 0 ? "" : cellValue),3}");
                if ((j + 1) % _grade == 0)
                    Console.Write(" |");
            }

            Console.WriteLine();

            if ((i + 1) % _grade == 0)
                Console.WriteLine("".PadRight(3 * _square + 2 * _grade + 1, '-'));
        }
    }

    private static void GenerateSudoku()
    {
        Console.WriteLine("Your sudoku is empty. Generating new sudoku...");
        _verbose = true;

        var sw = Stopwatch.StartNew();

        var fullSudoku = FindAllSolutions().First();
        Array.Copy(fullSudoku, _initialLayout, _fourth);

        var indexes = new FactorArray<int>(_fourth);
        for (var i = 0; i < _fourth; i++)
            indexes.Add(i);

        var digitsRemains = _fourth;
        var threshold = _grade switch
        {
            >= 2 and <= 4 => (int)(digitsRemains * 0.2),
            >= 5 and <= 6 => (int)(digitsRemains * 0.5),
            _ => (int)(digitsRemains * 0.6)
        };

        do
        {
            InitializeExactCoverTable();
            _solutionsCount = 0;

            var nextIndexIndex = new Random().Next(0, indexes.Count - 1);
            var nextIndex = indexes[nextIndexIndex];
            indexes.RemoveAt(nextIndexIndex);
            _initialLayout[nextIndex] = 0;
            digitsRemains--;

            RemoveKnownDigits();

            foreach (var _ in FindAllSolutions())
                if (_solutionsCount > 1)
                {
                    _initialLayout[nextIndex] = fullSudoku[nextIndex];
                    digitsRemains++;
                    break;
                }
        } while (indexes.Count > 0 && digitsRemains > threshold);

        sw.Stop();

        Console.WriteLine($"\nGeneration terminated due to '{(digitsRemains > threshold ? "indexes" : "threshold")}'." +
                          $" Digits remained {digitsRemains}");
        Console.WriteLine($"Sudoku {_square}x{_square} generated in {sw.ElapsedMilliseconds} ms.");
        Console.WriteLine($"Short form:\n{string.Join(' ', _initialLayout.Select(x => x == 0 ? "" : $"{x}"))}");
        Console.WriteLine("\nClassic form:");
        PrintSudoku(new int[_fourth]);
    }
}