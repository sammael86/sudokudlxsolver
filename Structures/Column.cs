namespace SudokuDLXSolver.Structures;

public class Column
{
    public readonly Node Head = new();
    public int Length;
    public long NameValue;
    public Column Prev = null!;
    public Column Next = null!;

    public int Name => (int)(NameValue >> 32);
    public int Value => (int)NameValue;
}