namespace SudokuDLXSolver.Structures;

public class FactorArray<T>
{
    private T[] _data;
    private int _count;
    private int _capacity;
    private int _factor = 2;

    public FactorArray(int capacity = 16)
    {
        _capacity = capacity;
        _data = new T[_capacity];
    }

    public int Count => _count;

    public void Add(T item)
    {
        if (_count == _capacity)
        {
            var newData = new T[_capacity * _factor + 1];
            Array.Copy(_data, newData, _count);
            _data = newData;
            _capacity = _data.Length;
        }

        _data[_count] = item;
        _count++;
    }

    public void RemoveAt(int index)
    {
        _count--;
        for (var i = index; i < _count; i++)
            _data[i] = _data[i + 1];
    }

    public void Clear() => _count = 0;

    public IEnumerator<T> GetEnumerator()
    {
        for (var i = 0; i < _count; i++)
            yield return _data[i];
    }

    public T this[int index] => _data[index];
}