namespace SudokuDLXSolver.Structures;

public class Node
{
    public Node Left = null!;
    public Node Right = null!;
    public Node Up = null!;
    public Node Down = null!;
    public Column Column = null!;
}